﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections.Generic;

namespace Bytemotiv.UI {

    public class BytemotivGridLayout : LayoutGroup {

        #region vars

        /*
        TODO: auto-        
        generates as many rows with the specified px width as needed to fit the content
        */
                
        private Dictionary<string, Rect> gridCells;

        private Vector2 fixedSizes;
        private Vector2 fractionSizes;

        private string[] templateColumns;
        private string[] templateRows;
        private string[] templateAreas;

        #endregion

        // ---------------------------------------------------------------------------------------

        #region functions 

        public override void CalculateLayoutInputHorizontal() {
            layout();
        }

        public override void CalculateLayoutInputVertical() {
            layout();
        }

        public override void SetLayoutHorizontal() {
            layout();
        }

        public override void SetLayoutVertical() {
            layout();
        }

        private void calculateBounds() {
            //bool isRelativeLayout = true;

            // calculate the fractions and percentages
            fixedSizes = new Vector2();
            fractionSizes = new Vector2();

            if (templateColumns == null) {
                return;
            }

            for (int i = 0; i < templateColumns.Length; i++) {
                if (templateColumns[i].Contains("px")) {
                    float width = parseSize(templateColumns[i], true);
                    fixedSizes.x += width;

                    //isRelativeLayout = false;
                }

                if (templateColumns[i].Contains("fr")) {
                    float fr = float.Parse(templateColumns[i].Replace("fr", ""));
                    fractionSizes.x += fr;
                }
            }

            for (int i = 0; i < templateRows.Length; i++) {
                if (templateRows[i].Contains("px")) {
                    float height = parseSize(templateRows[i], false);
                    fixedSizes.y += height;

                    //isRelativeLayout = false;
                }

                if (templateRows[i].Contains("fr")) {
                    float fr = float.Parse(templateRows[i].Replace("fr", ""));
                    fractionSizes.y += fr;
                }
            }

            /*
            if (!isRelativeLayout) {
                float width = 0.0F;
                for (int i = 0; i < templateColumns.Length; i++) {
                    width += parseSize(templateColumns[i], true);
                }

                float height = 0.0F;
                for (int i = 0; i < templateRows.Length; i++) {
                    height += parseSize(templateRows[i], false);
                }
                rectTransform.sizeDelta = new Vector2(width, height);
            }
            */
        }

        private void calculateGrid() {
            gridCells = new Dictionary<string, Rect>();

            float x = 0.0F;
            float y = 0.0F;

            if (templateAreas == null) {
                return;
            }

            for (int i = 0; i < templateAreas.Length; i++) {
                string[] rowAreas = templateAreas[i].Split(' ');
                for (int j = 0; j < rowAreas.Length; j++) {
                    string area = rowAreas[j];
                    float width = parseSize(templateColumns[j], true);
                    float height = parseSize(templateRows[i], false);

                    if (!gridCells.ContainsKey(area)) {
                        gridCells.Add(area, new Rect(x, y, width, height));
                    } else {
                        Rect areaRect = gridCells[area];
                        if (x != areaRect.x) {
                            if (areaRect.x + areaRect.width < rectTransform.rect.width) {
                                areaRect.width += width;
                            }
                        }
                        if (y != areaRect.y) {
                            if (areaRect.y + areaRect.height < rectTransform.rect.width) {
                                areaRect.height += height;
                            }
                        }
                        gridCells[area] = areaRect;
                    }
                    x += width;
                }
                x = 0.0F;
                y += parseSize(templateRows[i], false);
            }
        }

        private void layoutChildren() {
            if (gridCells == null) {
                calculateBounds();
                calculateGrid();
            }

            for (int i = 0; i < transform.childCount; i++) {
                BytemotivGridItem child = transform.GetChild(i).GetComponent<BytemotivGridItem>();
                if (child == null) {
                    continue;
                }
                if (gridCells.ContainsKey(child.getArea())) {
                    Rect childRect = gridCells[child.getArea()];
                    //child.setRect(childRect);

                    Vector4 padding = new Vector4(0.0F, 0.0F, 0.0F, 0.0F);
                    if (child.padding != null && child.padding.Length == 4) {
                        // top right bottom left
                        padding = new Vector4(child.padding[0], child.padding[1], child.padding[2], child.padding[3]);
                    }

                    SetChildAlongAxis(child.GetComponent<RectTransform>(), 0, childRect.xMin + padding[3], childRect.width - padding[1] - padding[3]);
                    SetChildAlongAxis(child.GetComponent<RectTransform>(), 1, childRect.yMin + padding[0], childRect.height - padding[2] - padding[0]);

                } else {
                    child.hide();
                }
            }
        }

        public void layout() {
            BytemotivGridBreakpoint[] breakpoints = GetComponents<BytemotivGridBreakpoint>();
            float gridWidth = rectTransform.rect.width;

            for (int i = 0; i < breakpoints.Length; i++) {
                //Debug.Log(breakpoints[i].breakpoint.x + " < " + gridWidth + " && " + gridWidth + " < " + breakpoints[i].breakpoint.y);
                if (breakpoints.Length == 1) {
                    templateColumns = breakpoints[0].templateColumns;
                    templateRows = breakpoints[0].templateRows;
                    templateAreas = breakpoints[0].templateAreas;
                } else {
                    if (breakpoints[i].breakpoint.x < gridWidth && gridWidth <= breakpoints[i].breakpoint.y) {
                        templateColumns = breakpoints[i].templateColumns;
                        templateRows = breakpoints[i].templateRows;
                        templateAreas = breakpoints[i].templateAreas;
                    }
                }
            }

            calculateBounds();
            calculateGrid();
            layoutChildren();        
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region helpers

        private float parseSize(string size, bool horizontal) {
            float calculatedSize = 0.0F;

            // pixel sizes
            if (size.Contains("px")) {
                calculatedSize = float.Parse(size.Replace("px", ""));
            }

            // fraction sizes
            else if (size.Contains("fr")) {
                float fraction = float.Parse(size.Replace("fr", ""));
                float totalSize;
                float totalFraction;

                if (horizontal) {
                    totalSize = rectTransform.rect.width - fixedSizes.x;
                    totalFraction = fractionSizes.x;
                } else {
                    totalSize = rectTransform.rect.height - fixedSizes.y;
                    totalFraction = fractionSizes.y;
                }
                calculatedSize = totalSize * fraction / totalFraction;
            }

            // percentage sizes
            else if (size.Contains("%")) {
                float percentage = float.Parse(size.Replace("%", ""));
                float totalSize;

                if (horizontal) {
                    totalSize = rectTransform.rect.width - fixedSizes.x;
                } else {
                    totalSize = rectTransform.rect.height - fixedSizes.y;
                }

                calculatedSize = totalSize / 100 * percentage;
            }             

            return calculatedSize;
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region unity

        protected override void OnRectTransformDimensionsChange() {
            layout();
        }

        protected override void OnValidate() {
            layout();
        }

        #endregion

    }

}