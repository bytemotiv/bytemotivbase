﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Bytemotiv.UI {

    public class BytemotivGridBreakpoint : BytemotivBehaviour {

        #region vars

        [SerializeField]
        public Vector2 breakpoint;

        [SerializeField]
        public string[] templateColumns;

        [SerializeField]
        public string[] templateRows;

        [SerializeField]
        public string[] templateAreas;

        #endregion

    }

}