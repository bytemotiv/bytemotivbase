﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Bytemotiv.UI {

    public class BytemotivGridItem : BytemotivBehaviour {

        #region vars

        [SerializeField]
        public string area;

        [SerializeField]
        public int[] padding;

        #endregion

        // ---------------------------------------------------------------------------------------

        #region functions

        public void hide() {
            rectTransform.sizeDelta = Vector2.zero;
        }

        public string getArea() {
            return area ?? "";
        }

        public void setRect(Rect rect) {
            rectTransform.anchorMin = new Vector2(0, 1);
            rectTransform.anchorMax = new Vector2(0, 1);
            rectTransform.anchoredPosition = new Vector2(rect.x, -rect.y);
            rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region unity

        #endregion

    }

}