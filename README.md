# BytemotivBehaviour

This repository contains the codebase we at [bytemotiv](http://www.bytemotiv.com) use for most of our Unity3D projects.

## Behaviours

Includes the basic (empty) *BytemotivBehaviour* class to replace *MonoBehaviour* as well as the methods grouped in separate files arranged in partial classes.

- *Components.cs* contains shortcuts to the most used components
- *Helper.cs* contains common methods to interact with the base class
- *UI.cs* contains helper methods and shortcuts for the UnityEngine.UI namespace

## Extensions

Includes the collection of type extensions we use frequently

- *ColorExtensions.cs* contains helper methods for color(space) operations
- *CurveExtensions.cs* contains helpers for manipulating and evaluating 2D curves
- *ListExtensions.cs* contains helpers for manipulating lists for example the Fisher-Yates shuffle
- *RectTransformExtensions.cs* contains helpers for manipulating the position of RectTransform components in the UI space
- *StringExtensions.cs* contains helpers for manipulating strings
- *UnityUIExtensions.cs* contains helpers for manipulating the state of objects in the UnityEngine.UI namespace

## I18N

Includes our I18N subsystem.
This is the most recent addition and still needs some work. It includes the *I18NController* responsible for reading and storing translations as well as the *I18NText* to be used on an UnityEngine.UI.Text component to give it I18N abilities

## Messaging

Includes the messaging subsystem we use for basically all data transfer between classes. Based on simple delegates.

## StateMachine

Includes a StateMachine based on the messaging subsystem.