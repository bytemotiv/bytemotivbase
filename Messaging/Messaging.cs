using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Bytemotiv {

    public class Messaging {

        #region vars

        public delegate void MessageDelegate(Message m);
        public MessageDelegate messagedelegate;
        private static Dictionary<string, MessageDelegate> messageHandlers;

        #endregion

        // ----------------------------------------------------------------------------------------------------------------

        #region functions

        private static void initMessageHandlers() {
            messageHandlers = messageHandlers ?? new Dictionary<string, MessageDelegate>();
        }

        public static void subscribe(string messageid, MessageDelegate handler) {
            initMessageHandlers();
            if (!messageHandlers.ContainsKey(messageid)) {
                messageHandlers.Add(messageid, null);
            }
            messageHandlers[messageid] += handler;
        }

        public static void unsubscribe(string messageid, MessageDelegate handler) {
            initMessageHandlers();
            if (messageHandlers.ContainsKey(messageid)) {
                messageHandlers[messageid] -= handler;
            }
        }

        public static void broadcast(string messageid) {
            Message message = new Message(messageid);
            broadcast(message);
        }

        public static void broadcast(Message message) {
            if (messageHandlers != null && messageHandlers.ContainsKey(message.messageid)) {
                //try {
                messageHandlers[message.messageid](message);
                //} catch (System.NullReferenceException e) {}
            }
        }

        #endregion

    }


    public partial class BytemotivBehaviour {

        internal static Dictionary<string, Messaging.MessageDelegate> _handlers;

        public static void subscribe(string messageId, Messaging.MessageDelegate handler) {
            Messaging.subscribe(messageId, handler);
        }

        public static void unsubscribe(string messageId, Messaging.MessageDelegate handler) {
            Messaging.unsubscribe(messageId, handler);
        }

        public static void broadcast(string messageid) {
            Messaging.broadcast(messageid);
        }

        public static void broadcast(Message message) {
            Messaging.broadcast(message);
        }

    }

}
