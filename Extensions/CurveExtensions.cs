﻿using UnityEngine;
using System.Collections;

public static class CurveExtensions {

    public static float getDuration(this AnimationCurve curve) {
        return curve[curve.length-1].time;
    }

}

