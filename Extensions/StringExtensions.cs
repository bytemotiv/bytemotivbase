using UnityEngine;

public static class StringExtensions {

	public static string Cleanup(this string str) {
		str = str.Replace ("&auml;", "�");
		str = str.Replace ("&ouml;", "�");
		str = str.Replace ("&uuml;", "�");
		str = str.Replace ("&Auml;", "�");
		str = str.Replace ("&Ouml;", "�");
		str = str.Replace ("&Uuml;", "�");
		str = str.Replace ("&szlig;", "�");

		str = str.Replace ("&raquo;", "�");
		str = str.Replace ("&laquo;", "�");

		str = str.Replace ("<br />", "\n");
		str = str.Replace ("<br/>", "\n");
		str = str.Replace ("<br >", "\n");
		str = str.Replace ("<br>", "\n");

		str = str.Replace ("  ", " ");
		str = str.Trim();
		return str;
	}
}

