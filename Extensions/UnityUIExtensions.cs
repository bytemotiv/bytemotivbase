using UnityEngine;
using UnityEngine.UI;

public static class UnityUIExtensions {

    public static void Show(this CanvasGroup canvasGroup) {
        if (canvasGroup != null) {
            canvasGroup.alpha = 1.0F;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }
    }

    public static void Hide(this CanvasGroup canvasGroup) {
        if (canvasGroup != null) {
            canvasGroup.alpha = 0.0F;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }
    }

    public static Sprite ToSprite(this Texture2D texture) {
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        return sprite;
    }
}