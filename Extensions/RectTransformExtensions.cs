﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RectTransformExtensions {

    public static void SetLeft(this RectTransform rect, float left) {
        rect.offsetMin = new Vector2(left, rect.offsetMin.y);
    }

    public static void SetRight(this RectTransform rect, float right) {
        rect.offsetMax = new Vector2(-right, rect.offsetMax.y);
    }

    public static void SetTop(this RectTransform rect, float top) {
        rect.offsetMax = new Vector2(rect.offsetMax.x, top);
    }

    public static void SetBottom(this RectTransform rect, float bottom) {
        rect.offsetMin = new Vector2(rect.offsetMin.x, bottom);
    }


}
