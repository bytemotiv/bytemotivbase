﻿using UnityEngine;
using System.Collections;

namespace Bytemotiv {

    public partial class BytemotivBehaviour : MonoBehaviour {

        public virtual void Off() {
            gameObject.SetActive(false);
        }

        public virtual void On() {
            gameObject.SetActive(true);
        }
    }

}
