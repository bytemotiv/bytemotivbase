﻿using UnityEngine;
using UnityEngine.UI;

namespace Bytemotiv {

    public partial class BytemotivBehaviour : MonoBehaviour {

        #region UI Components

        private RectTransform _rectTransform = null;
        public RectTransform rectTransform {
            get {
                return _rectTransform ? _rectTransform : _rectTransform = (RectTransform)transform;
            }
        }

        private Button _button = null;
        public Button button {
            get {
                return _button ? _button : _button = GetComponent<Button>();
            }
        }

        private Text _Text = null;
        public Text Text {
            get {
                return _Text ? _Text : _Text = GetComponent<Text>();
            }
        }

        public string text {
            get {
                return Text.text;
            }
            set {
                Text.text = value;
            }
        }

        private Image _image = null;
        public Image image {
            get {
                return _image ? _image : _image = GetComponent<Image>();
            }
        }

        private RawImage _rawimage = null;
        public RawImage rawimage {
            get {
                return _rawimage ? _rawimage : _rawimage = GetComponent<RawImage>();
            }
        }

        #endregion

    }
}