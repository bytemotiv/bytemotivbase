﻿using UnityEngine;
using System.Collections;

namespace Bytemotiv {

    public partial class BytemotivBehaviour : MonoBehaviour {

        #region Components 

        private Rigidbody _rigidbody = null;
        public new Rigidbody rigidbody {
            get {
                return _rigidbody ? _rigidbody : _rigidbody = GetComponent<Rigidbody>();
            }
        }

        private AudioSource _audio = null;
        public new AudioSource audio {
            get {
                return _audio ? _audio : _audio = GetComponent<AudioSource>();
            }
        }

        private Collider _collider = null;
        public new Collider collider {
            get {
                return _collider ? _collider : _collider = GetComponent<Collider>();
            }
        }

        private Renderer _renderer = null;
        public new Renderer renderer {
            get {
                return _renderer ? _renderer : _renderer = GetComponent<Renderer>();
            }
        }
        
        private MeshFilter _meshfilter = null;
        public MeshFilter meshfilter {
            get {
                return _meshfilter ? _meshfilter : _meshfilter = GetComponent<MeshFilter>();
            }
        }

        private Texture2D _texture = null;
        public Texture2D texture {
            get {
                return _texture ? _texture : _texture = GetComponent<Texture2D>();
            }
        }

        #endregion

    }
}