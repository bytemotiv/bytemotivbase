﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Bytemotiv {

    public class StateMachine {

        #region vars

        public delegate void StateDelegate();
        private static Dictionary<string, Dictionary<string, StateDelegate>> stateEnterHandlers;
        private static Dictionary<string, Dictionary<string, StateDelegate>> stateExitHandlers;

        private static string currentState;
        private static string currentMode;

        #endregion

        // ----------------------------------------------------------------------------------------------------------------

        #region functions

        private static void initStateHandlers() {
            stateEnterHandlers = stateEnterHandlers ?? new Dictionary<string, Dictionary<string, StateDelegate>>();
            stateExitHandlers = stateExitHandlers ?? new Dictionary<string, Dictionary<string, StateDelegate>>();
            currentState = "";
            currentMode = "";
        }

        public static void OnEnterState(string state, string mode, StateDelegate stateDelegate) {
            initStateHandlers();
            if (!stateEnterHandlers.ContainsKey(state)) {
                stateEnterHandlers.Add(state, new Dictionary<string, StateDelegate>() { { mode, null }});
            }
            stateEnterHandlers[state][mode] += stateDelegate;
        }

        public static void OnExitState(string state, string mode, StateDelegate stateDelegate) {
            initStateHandlers();
            if (!stateExitHandlers.ContainsKey(state)) {
                stateExitHandlers.Add(state, new Dictionary<string, StateDelegate>() { { mode, null } });
            }
            stateExitHandlers[state][mode] += stateDelegate;
        }

        public static void RemoveEnterState(string state, string mode, StateDelegate stateDelegate) {
            initStateHandlers();
            if (stateEnterHandlers.ContainsKey(state)) {
                stateEnterHandlers[state][mode] -= stateDelegate;
            }
        }

        public static void RemoveExitState(string state, string mode, StateDelegate stateDelegate) {
            initStateHandlers();
            if (stateExitHandlers.ContainsKey(state)) {
                stateExitHandlers[state][mode] -= stateDelegate;
            }
        }

        public static string GetState() {
            return currentState;
        }

        public static string GetMode() {
            return currentMode;
        }

        public static void ExitState() {
            if (stateExitHandlers != null && stateExitHandlers.ContainsKey(currentState)) {
                stateExitHandlers[currentState][currentMode]();
            }
        }

        public static void EnterState(string state, string mode) {
            currentState = state;
            currentMode = mode;

            if (stateEnterHandlers != null && stateEnterHandlers.ContainsKey(currentState)) {
                stateEnterHandlers[currentState][mode]();
            }
        }

        #endregion

    }


    public partial class BytemotivBehaviour {

        public static void OnEnterState(string state, StateMachine.StateDelegate handler) {
            StateMachine.OnEnterState(state, "", handler);
        }

        public static void OnEnterState(string state, string mode, StateMachine.StateDelegate handler) {
            StateMachine.OnEnterState(state, mode, handler);
        }

        public static void OnExitState(string state, StateMachine.StateDelegate handler) {
            StateMachine.OnExitState(state, "", handler);
        }

        public static void OnExitState(string state, string mode, StateMachine.StateDelegate handler) {
            StateMachine.OnExitState(state, mode, handler);
        }

        public static void RemoveEnterState(string state, StateMachine.StateDelegate handler) {
            StateMachine.RemoveEnterState(state, "", handler);
        }

        public static void RemoveEnterState(string state, string mode, StateMachine.StateDelegate handler) {
            StateMachine.RemoveEnterState(state, mode, handler);
        }

        public static void RemoveExitState(string state, StateMachine.StateDelegate handler) {
            StateMachine.RemoveExitState(state, "", handler);
        }

        public static void RemoveExitState(string state, string mode, StateMachine.StateDelegate handler) {
            StateMachine.RemoveExitState(state, mode, handler);
        }

        public void EnterState(string state) {           
            StartCoroutine(switchState(state, ""));
        }

        public void EnterState(string state, string mode) {
            StartCoroutine(switchState(state, mode));
        }

        public static string GetState() {
            return StateMachine.GetState();
        }

        public static string GetMode() {
            return StateMachine.GetMode();
        }

        internal IEnumerator switchState(string state, string mode) {
            StateMachine.ExitState();
            yield return 0; // wait one frame
            StateMachine.EnterState(state, mode);
        }

    }

}
