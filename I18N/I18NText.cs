﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Bytemotiv.I18N {

    public class I18NText : BytemotivBehaviour {

        #region vars

        [SerializeField]
        private string key;

        private Text uiText;

        #endregion

        // ---------------------------------------------------------------------------------------

        #region functions

        public void refresh(string newKey) {
            key = newKey;
            refresh();
        }

        public void refresh() {
            StartCoroutine(requestTranslation());
        }

        private IEnumerator requestTranslation() {
            yield return new WaitForSeconds(0.01F);
            broadcast(new Message("language") { { "action", "request" }, { "key", key } });
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region handlers

        private void handleLanguage(Message message) {
            string action = message.getString("action");
            switch (action) {
                case "switch":
                    StartCoroutine(requestTranslation());
                    break;
                case "update":
                    string updateKey = message.getString("key");
                    if (updateKey == key) {
                        string translation = message.getString("value");
                        translation = translation.Replace("\\n", "\n");
                        translation = translation.Replace("\\t", "\t");
                        uiText.text = translation;
                    }
                    break;
            }
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region unity

        private void Awake() {
            uiText = GetComponent<Text>();
            subscribe("language", handleLanguage);
        }

        private void OnDestroy() {
            unsubscribe("language", handleLanguage);
        }

        #endregion

    }

}
