﻿using UnityEngine;
using System.Collections.Generic;

namespace Bytemotiv.I18N {

    public class I18NController : BytemotivBehaviour {

        #region vars

        private Dictionary<string, Dictionary<string, string>> translations;
        private string currentLanguage;

        #endregion

        // ---------------------------------------------------------------------------------------

        #region functions

        public string get(string key) {
            string translation = get(currentLanguage, key, "");
            translation = translation.Replace("\\t", "\t");
            translation = translation.Replace("\\n", "\n");
            return translation;
        }

        public string get(string language, string key) {
            return get(language, key, "");
        }

        private string get(string language, string key, string fallback) {
            if (translations.ContainsKey(language)) {
                if (translations[language].ContainsKey(key)) {
                    return translations[language][key];
                }
            }
            if (fallback != "") {
                return fallback;
            }
            Debug.LogWarning("Missing [" + language + "] Translation: " + key);
            return key;
        }

        private void loadLanguages() {
            translations = new Dictionary<string, Dictionary<string, string>>();

            TextAsset[] languageFiles = Resources.LoadAll<TextAsset>("Languages");
            for (int i=0; i < languageFiles.Length; i++) {
                parseLanguageFile(languageFiles[i].name, languageFiles[i].text);
            }
        }

        private void parseLanguageFile(string languageIdentifier, string languageFile) {
            Dictionary<string, string> languageTranslations = new Dictionary<string, string>();

            string[] lines = languageFile.Split('\n');
            for (int i=0; i < lines.Length; i++) {
                if (lines[i].StartsWith("#") || lines[i].Length <= 1) {
                    continue;
                }
                string[] translation = lines[i].Split(';');
                if (translation.Length == 2) {
                    if (!languageTranslations.ContainsKey(translation[0])) {
                        languageTranslations.Add(translation[0], translation[1]);
                    }
                }
            }
            translations.Add(languageIdentifier, languageTranslations);

            string localLanguageIdentifier = get(languageIdentifier, "localLanguageShort");
            broadcast(new Message("language") { { "action", "loaded" }, { "name", languageIdentifier }, { "localName", localLanguageIdentifier } });
        }

        private void loadSystemLanguage() {
            string targetLanguage = Application.systemLanguage.ToString();
            if (!translations.ContainsKey(targetLanguage)) {
                Debug.LogWarning("Language [" + targetLanguage + "] not found");
                targetLanguage = SystemLanguage.English.ToString();
            }
            currentLanguage = targetLanguage;

            broadcast(new Message("language") { { "action", "switch" }, { "language", targetLanguage } });
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region handlers

        private void handleLanguage(Message message) {
            string action = message.getString("action");
            switch (action) {
                case "switch":
                    string targetLanguage = message.getString("language");
                    currentLanguage = targetLanguage;
                    break;
                case "request":
                    string key = message.getString("key");
                    broadcast(new Message("language") { { "action", "update" }, { "key", key }, { "value", get(key) } });
                    break;
            }
        }

        #endregion

        // ---------------------------------------------------------------------------------------

        #region unity

        private void Awake() {
            DontDestroyOnLoad(gameObject);

            subscribe("language", handleLanguage);
        }

        private void Start() {
            loadLanguages();
            loadSystemLanguage();
        }

        private void OnDestroy() {
            unsubscribe("language", handleLanguage);
        }

        #endregion

    }

}
